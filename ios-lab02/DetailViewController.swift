//
//  DetailViewController.swift
//  ios-lab02
//
//  Created by Alex Söderberg on 2018-12-09.
//  Copyright © 2018 Alex Söderberg. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var navItemLabel: UINavigationItem!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var experienceNameLabel: UILabel!
    @IBOutlet weak var experienceDateLabel: UILabel!
    @IBOutlet weak var experienceInfoLabel: UILabel!
    
    var experience: Experience = Experience(imageName: "", experienceName: "", experienceDate: "", experienceInfo: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navItemLabel.title = experience.experienceName
        imageView.image = UIImage(named: experience.imageName)
        experienceNameLabel.text = experience.experienceName
        experienceDateLabel.text = experience.experienceDate
        experienceInfoLabel.text = experience.experienceInfo
        
        imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
        imageView.clipsToBounds = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
