//
//  ExperienceViewController.swift
//  ios-lab02
//
//  Created by Alex Söderberg on 2018-12-09.
//  Copyright © 2018 Alex Söderberg. All rights reserved.
//

import UIKit



class ExperienceViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    var experiences: [[Experience]] = [[]]
    let sections = ["Work", "Education"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        experiences = [
            [
                Experience(imageName: "firstImage", experienceName: "firstExperience", experienceDate: "2018-11-10", experienceInfo: "This is the first work experience"),
                Experience(imageName: "firstImage", experienceName: "secondExperience", experienceDate: "2018-11-12", experienceInfo: "This is the second work experience")
            ],
            [
                Experience(imageName: "secondImage", experienceName: "thirdExperience", experienceDate: "2018-11-13", experienceInfo: "This is the first education experience"),
                Experience(imageName: "secondImage", experienceName: "fourthExperience", experienceDate: "2018-11-17", experienceInfo: "This is the second education experience")
            ]
        ]
        
        
        // Do any additional setup after loading the view.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailViewController {
            if let indexPath = sender as? IndexPath {
                let experience = experiences[indexPath.section][indexPath.row]
                destination.experience = experience
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ExperienceViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experiences.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceTableViewCell", for: indexPath) as? ExperienceTableViewCell {
            let experience = experiences[indexPath.section][indexPath.row]
            cell.experienceNameLabel.text = experience.experienceName
            cell.experienceDateLabel.text = experience.experienceDate
            cell.experienceImage.image = UIImage(named: experience.imageName)
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return self.sections[section]
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30))
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 0, y: 0, width: headerView.frame.width - 16, height: 30))
        
        headerLabel.text = sections[section]
        headerLabel.textColor = UIColor.black
        headerLabel.font = UIFont.boldSystemFont(ofSize: 14)
        headerLabel.textAlignment = .center
        headerView.addSubview(headerLabel)
        
        
        
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailSegue", sender: indexPath)
    }
}
