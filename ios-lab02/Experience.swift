//
//  Experience.swift
//  ios-lab02
//
//  Created by Alex Söderberg on 2018-12-09.
//  Copyright © 2018 Alex Söderberg. All rights reserved.
//

import Foundation

struct Experience {
    var imageName: String
    var experienceName: String
    var experienceDate: String
    var experienceInfo: String
}
