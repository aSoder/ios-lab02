//
//  SkillsViewController.swift
//  ios-lab02
//
//  Created by Alex Söderberg on 2018-12-09.
//  Copyright © 2018 Alex Söderberg. All rights reserved.
//

import UIKit

class SkillsViewController: UIViewController {

    @IBOutlet weak var viewBox: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewBox.layer.cornerRadius = viewBox.layer.frame.size.width/2
        viewBox.layer.masksToBounds = true
        
        
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func animateBtnDidTouch(_ sender: UIButton){
        UIView.animate(withDuration: 1, animations:{
            self.viewBox.backgroundColor = .yellow
            self.viewBox.frame.origin.y = 100
        }, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
