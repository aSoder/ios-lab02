//
//  ViewController.swift
//  ios-lab02
//
//  Created by Alex Söderberg on 2018-12-07.
//  Copyright © 2018 Alex Söderberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var skillsButton: UIButton!
    @IBOutlet weak var experienceButton: UIButton!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        experienceButton.layer.cornerRadius = 25
        skillsButton.layer.cornerRadius = 25
        imageView.layer.cornerRadius = imageView.layer.frame.size.width / 2
        imageView.clipsToBounds = true
        
        // Do any additional setup after loading the view, typically from a nib.
    }


}

